﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public static class loadinfo
{
    public static InputDevice davidControls { get; set; } = null;
    public static InputDevice goliathControls { get; set; } = null;
}
