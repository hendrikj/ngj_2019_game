﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    private WaitForSeconds killme = new WaitForSeconds(Constants.GOLIATH_FIREBALL_SELFDESTRUCT);

    public IEnumerator Selfdestruct()
    {
        yield return killme;
        Util.DeactivateAndDestroy(gameObject);
    }

    private void Awake()
    {
        StartCoroutine(Selfdestruct());
    }

    private void FixedUpdate()
    {
        transform.LeanSetLocalPosX(
            transform.localPosition.x
            + Constants.GOLIATH_FIREBALL_SPEED * Time.fixedDeltaTime);
    }
}
