﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParallaxController : MonoBehaviour
{
    [SerializeField] private FloatVariable scrollSpeed;

    [SerializeField] private GameObject behindSkyElement;
    [SerializeField] private GameObject currentSkyElement;
    [SerializeField] private GameObject frontSkyElement;

    [SerializeField] private List<GameObject> clouds_level_1;
    [SerializeField] private List<GameObject> clouds_level_2;
    
    private bool running = false;

    private void FixedUpdate()
    {
        if (!running) return;

        moveSkyElement(behindSkyElement);
        moveSkyElement(currentSkyElement);
        moveSkyElement(frontSkyElement);

        if(isOffscreen(behindSkyElement))
        {
            moveToFront(behindSkyElement);
            swapSkyElements();
        }

        foreach (GameObject cloud in clouds_level_1)
        {
            moveBackgroundElement(cloud, Constants.BACKGROUND_CLOUD_1_SCROLL_FACTOR);
            if (isOffscreen(cloud)){
                moveToFront(cloud);
            }
        }

        foreach (GameObject cloud in clouds_level_2)
        {
            moveBackgroundElement(cloud, Constants.BACKGROUND_CLOUD_2_SCROLL_FACTOR);
            if (isOffscreen(cloud)){
                moveToFront(cloud);
            }
        }

    }

    private void moveSkyElement(GameObject skyElement)
    {
        moveBackgroundElement(skyElement, Constants.BACKDROP_SKY_SCROLL_FACTOR);
    }

    private void moveBackgroundElement(GameObject element, float factor)
    {
        element.transform.LeanSetLocalPosX(
            element.transform.localPosition.x
            - scrollSpeed.value * factor * Time.fixedDeltaTime);

    }

    private void swapSkyElements()
    {
        GameObject buffer = behindSkyElement;
        behindSkyElement = currentSkyElement;
        currentSkyElement = frontSkyElement;
        frontSkyElement = buffer;
    }

    public void OnGameStart()
    {
        running = true;
    }

    private bool isOffscreen(GameObject element)
    {
        return element.transform.localPosition.x < Constants.BACKDROP_SKY_LENGTH * -1;
    }

    private void moveToFront(GameObject element)
    {
        element.transform.LeanSetLocalPosX(
            element.transform.localPosition.x
            + 3 * Constants.BACKDROP_SKY_LENGTH);
    }
}
