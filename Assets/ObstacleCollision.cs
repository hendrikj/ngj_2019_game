﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollision : MonoBehaviour
{
    [SerializeField] private FloatVariable penalty;


    private void OnTriggerEnter(Collider other)
    {
        other.SendMessage("ObstacleCollision", penalty.value);
    }
}
