﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLoseController : MonoBehaviour
{
    public void OnDavidGotCaught()
    {
        SceneManager.LoadScene(Constants.SCENE_WIN_GOLIATH);
    }

    public void OnDavidWins()
    {
        SceneManager.LoadScene(Constants.SCENE_WIN_DAVID);
    }
}
