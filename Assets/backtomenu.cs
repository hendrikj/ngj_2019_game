﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class backtomenu : MonoBehaviour
{
    private WaitForSeconds delay = new WaitForSeconds(6f);


    public IEnumerator GoBack()
    {
        yield return delay;
        SceneManager.LoadScene(Constants.SCENE_MENU);
    }

    private void Awake()
    {
        StartCoroutine(GoBack());
    }

}
