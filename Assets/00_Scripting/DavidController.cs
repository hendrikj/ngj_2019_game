﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DavidController : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private GameObject shieldEffect;
    [SerializeField] private GameEvent leadMinViolation;
    [SerializeField] private GameEvent davidWinEvent;
    [SerializeField] private GameEvent stopScrolling;

    private Text manaDisplay;

    private int currentLane = 1;
    private bool isSwitchingLane = false;
    private float target_z = 0;

    private float mana = Constants.DAVID_MANA_STARTING;

    private WaitForSeconds shieldDurationDelay = new WaitForSeconds(Constants.DAVID_SHIELD_DURATION);
    private WaitForSeconds winDelay = new WaitForSeconds(Constants.WIN_DELAY);


    private bool grounded = true;
    private bool isJumping = false;
    private float jumpTime = 0f;

    private float jump_speed_up = Constants.DAVID_JUMP_HEIGHT / Constants.DAVID_JUMP_APEX;
    private float jump_speed_down = Constants.DAVID_JUMP_HEIGHT / (Constants.DAVID_JUMP_DURATION - Constants.DAVID_JUMP_APEX);

    private bool isShieldActive = false;
    private float invulnerable_until = 0f;

    private void Update()
    {
        Vector3 position = transform.position;

        if (isSwitchingLane)
        {
            float current_z = position.z;

            if (current_z < target_z)
            {
                float next_z = current_z + Constants.DAVID_LANESWITCH_SPEED * Time.deltaTime;
                if (next_z > target_z)
                {
                    next_z = target_z;
                    isSwitchingLane = false;
                }
                position = new Vector3(position.x, position.y, next_z);
            }
            else
            {
                float next_z = current_z - Constants.DAVID_LANESWITCH_SPEED * Time.deltaTime;
                if (next_z < target_z)
                {
                    next_z = target_z;
                    isSwitchingLane = false;
                }
                position = new Vector3(position.x, position.y, next_z);
            }
        }

        if (isJumping)
        {
            if(Time.time < jumpTime + Constants.DAVID_JUMP_APEX)
            {
                position = position + new Vector3(0, jump_speed_up, 0) * Time.deltaTime;
            }
            else
            {
                position = position - new Vector3(0, jump_speed_down, 0) * Time.deltaTime;
                if(position.y<=0)
                {
                    position = new Vector3(
                        position.x,
                        0,
                       position.z
                        );
                    grounded = true;
                    isJumping = false;
                    animator.ResetTrigger("jumping");
                    animator.SetTrigger("running");
                }
            }
        }
        
        if(transform.localPosition.x < Constants.DAVID_MAX_LEAD)
        {
            position = position + new Vector3(Constants.DAVID_RUNSPEED_BONUS, 0, 0) * Time.deltaTime; 
        }


        if (transform.localPosition.x <= Constants.DAVID_MIN_LEAD) leadMinViolation?.Invoke();

        transform.position = position;

    }

    public void OnGameStart() 
    {
        animator.SetTrigger("running");
    }

    public void OnMoveUp()
    {
        if (currentLane < Constants.MAX_LANE)
        {
            MoveToLane(currentLane + 1);
        }
    }

    public void OnMoveDown()
    {
        if (currentLane > Constants.MIN_LANE)
        {
            MoveToLane(currentLane - 1);
        }
    }

    public void OnJump()
    {
        Debug.Log("David: Jump");
        if (grounded && !isJumping && !isSwitchingLane)
        {
            jumpTime = Time.time;
            isJumping = true;
            grounded = false;
            animator.ResetTrigger("running");
            animator.SetTrigger("jumping");
        }
    }

    public void OnSlide()
    {
        if (mana < Constants.DAVID_ATTACK_MANA_COST) return;
        mana = 0;
        Debug.Log("David: Finisher");

        stopScrolling?.Invoke();
        animator.ResetTrigger("running");
        animator.SetTrigger("finish");

        StartCoroutine(win());


    }

    public IEnumerator win()
    {
        yield return winDelay;
        davidWinEvent.Invoke();

    }

    public void OnFinishingattack()
    {
        Debug.Log("David: Slide");
    }


    public void OnShield()
    {
        if (mana < Constants.DAVID_SHIELD_MANA_COST) return;
        if (isShieldActive) return;
    
        mana = mana - Constants.DAVID_SHIELD_MANA_COST;
        invulnerable_until = Time.time + Constants.DAVID_SHIELD_DURATION;

        isShieldActive = true;
        shieldEffect.SetActive(true);
        StartCoroutine(DeactivateShield());
        UpdateManaDisplay();

    }

    private void MoveToLane(int lane)
    {
        if (isSwitchingLane) return;
        if (isJumping) return;

        currentLane = lane;
        isSwitchingLane = true;
        target_z = currentLane * Constants.LANE_WIDTH + Constants.DAVID_LANE_OFFSET;
    }

    public void ObstacleCollision(float penalty)
    {
        ApplyPenalty(penalty);
    }

    public void ApplyPenalty(float penalty)
    {
        if (invulnerable_until < Time.time)
        {
            LeanTween.moveLocalX(
                gameObject,
                transform.localPosition.x - penalty,
                Constants.DAVID_PENALTY_DURATION);

            invulnerable_until = Time.time + Constants.DAVID_HIT_INVULNERABILITY_DURATION;
        }
    }

    public void PickupMana(float amount)
    {
        mana = Mathf.Min(mana + amount, Constants.DAVID_MANA_MAX);
        UpdateManaDisplay();
    }

    public IEnumerator DeactivateShield()
    {
        yield return shieldDurationDelay;
        isShieldActive = false;
        shieldEffect.SetActive(false);
    }

    private void UpdateManaDisplay()
    {
        string tes = String.Format("Mana:{0}/{1}",mana, Constants.DAVID_MANA_MAX );
        manaDisplay.text = tes;
    }

    private void Awake()
    {
        manaDisplay = GameObject.FindGameObjectWithTag("mana").GetComponent<Text>();
    }

}
