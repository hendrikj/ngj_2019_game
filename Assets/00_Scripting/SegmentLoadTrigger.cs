﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentLoadTrigger : MonoBehaviour
{
    [SerializeField] private GameEvent triggerEvent;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Ground: SEGMENT LOAD TRIGGERED");
        triggerEvent?.Invoke();
    }
}
