﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoliathController : MonoBehaviour
{

    [SerializeField] private Transform fireBallSpawnpoint;
    [SerializeField] private GameObject FireBallPrefab;

    private Transform movementRoot;

    private int currentLane = 1;
    private bool isSwitchingLane = false;
    private float target_z = 0;

    private bool isFireballReady = true;
    private WaitForSeconds fireballCoolDown = new WaitForSeconds(Constants.GOLIATH_FIREBALL_COOLDOWN);

    private void Awake()
    {
        movementRoot = GameObject.FindGameObjectWithTag(Constants.TAG_MOVEMENTROOT).transform;
    }

    private void Update()
    {
        if (isSwitchingLane)
        {
            float current_z = transform.position.z;

            if (current_z < target_z)
            {
                float next_z = current_z + Constants.GOLIATH_LANESWITCH_SPEED * Time.deltaTime;
                if (next_z > target_z)
                {
                    next_z = target_z;
                    isSwitchingLane = false;
                }
                transform.position = new Vector3(transform.position.x, transform.position.y, next_z);
            }
            else
            {
                float next_z = current_z - Constants.GOLIATH_LANESWITCH_SPEED * Time.deltaTime;
                if (next_z < target_z)
                {
                    next_z = target_z;
                    isSwitchingLane = false;
                }
                transform.position = new Vector3(transform.position.x, transform.position.y, next_z);
            }
        }
    }

    public void OnFireBall()
    {
        if (isFireballReady)
        {
            GameObject fireball = Instantiate(FireBallPrefab);
            fireball.transform.position = fireBallSpawnpoint.position;
            fireball.transform.parent = movementRoot;

            isFireballReady = false;
            StartCoroutine(ResetFireball());
        }
    }

    public void OnMoveUp()
    {
        if (currentLane < Constants.MAX_LANE)
        {
            MoveToLane(currentLane + 1);
        }
    }

    public void OnMoveDown()
    {
        if (currentLane > Constants.MIN_LANE)
        {
            MoveToLane(currentLane - 1);
        }
    }

    private void MoveToLane(int lane)
    {
        if (isSwitchingLane) return;

        currentLane = lane;
        isSwitchingLane = true;
        target_z = currentLane * Constants.LANE_WIDTH;
    }

    public IEnumerator ResetFireball()
    {
        yield return fireballCoolDown;
        isFireballReady = true;
    }

}
