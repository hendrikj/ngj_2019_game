﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util : MonoBehaviour
{
    public static void DeactivateAndDestroy(GameObject target)
    {
        target.SetActive(false);
        Destroy(target);
    }
}
