﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public static bool DEBUG = true;


    public static string SCENE_MENU = "Menu";
    public static string SCENE_MAIN = "InputTest 1";
    public static string SCENE_WIN_GOLIATH = "GoliathWins";
    public static string SCENE_WIN_DAVID = "DavidWins";


    public static string TAG_MOVEMENTROOT = "movementroot";

    // Lanes
    public static int NUM_LANES = 4;
    public static int MAX_LANE = 3;
    public static int MIN_LANE = 0;
    public static float LANE_0_Y = 0;
    public static float LANE_WIDTH = 3;
    public static float LANE_LENGTH = 210;

    // Players
    public static int MAX_PLAYERS = 2;

    public static float WIN_DELAY = 5f;

    // David
    public static float DAVID_MANA_STARTING = 0f;
    public static float DAVID_MANA_MAX = 100f;
    public static float DAVID_MIN_LEAD = -25f;
    public static float DAVID_MAX_LEAD = 34f;
    public static float DAVID_RUNSPEED_BONUS = 0.1f;
    public static float DAVID_LANESWITCH_SPEED = 3 * LANE_WIDTH;
    public static float DAVID_JUMP_HEIGHT = 3f;
    public static float DAVID_JUMP_DURATION = 0.7f;
    public static float DAVID_JUMP_APEX = 0.65f * DAVID_JUMP_DURATION;
    public static float DAVID_LANE_OFFSET = 0f;
    public static float DAVID_HIT_INVULNERABILITY_DURATION = 2f;
    public static float DAVID_PENALTY_DURATION = DAVID_HIT_INVULNERABILITY_DURATION;
    public static float DAVID_SHIELD_DURATION = 2f;
    public static float DAVID_SHIELD_MANA_COST = 10f;
    public static float DAVID_ATTACK_MANA_COST = 100f;

    // Goliath
    public static float GOLIATH_LANESWITCH_SPEED = 2 * LANE_WIDTH;
    public static float GOLIATH_FIREBALL_SPEED = 11f;
    public static float GOLIATH_FIREBALL_COOLDOWN = 2.5f;
    public static float GOLIATH_FIREBALL_SELFDESTRUCT = 15f;

    // Mana Pickup
    public static float MANAPICKUP_AMOUNT = 15f;
    public static float MANAPICKUP_SIN_FLOAT_INTENSITY = 0.5f;
    public static float MANAPICKUP_SIN_FLOAT_SPEED = 1f;

    // Shadows
    public static float SHADOW_HEIGHT = 0.01f;

    // Background
    public static float BACKDROP_SKY_LENGTH = 281.3f;
    public static float BACKDROP_SKY_SCROLL_FACTOR = 1f / 7f;

    
    public static float BACKGROUND_CLOUD_1_SCROLL_FACTOR = 1f / 5f;
    public static float BACKGROUND_CLOUD_2_SCROLL_FACTOR = 1f / 4.5f;


    //58
}