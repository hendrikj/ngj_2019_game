﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPickupSinFloat : MonoBehaviour
{

    private float baseHeight;


    private void Awake()
    {
        baseHeight = transform.position.y;
    }


    private void FixedUpdate()
    {
        transform.position = new Vector3(
            transform.position.x,
            baseHeight + Mathf.Sin(Time.time * Constants.MANAPICKUP_SIN_FLOAT_SPEED) * Constants.MANAPICKUP_SIN_FLOAT_INTENSITY,
            transform.position.z);
    }
}
