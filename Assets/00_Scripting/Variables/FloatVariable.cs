﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Float Variable", menuName = "Variable/Float", order = 54)]
public class FloatVariable : ScriptableObject
{
    public float value;
}
