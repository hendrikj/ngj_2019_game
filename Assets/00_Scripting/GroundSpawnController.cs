﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpawnController : MonoBehaviour
{
    [SerializeField] private GameObject initialGroundSegment;
    [SerializeField] private List<GameObject> groundSegmentPrefabs;


    private System.Random rng = new System.Random();

    private GameObject previousSegment = null;
    private GameObject currentSegment = null;

    private void Awake()
    {
        currentSegment = initialGroundSegment;
    }

    public void SpawnNextSegment()
    {
        int nextSegmentIndex = rng.Next(groundSegmentPrefabs.Count);
        GameObject nextSegment = Instantiate(groundSegmentPrefabs[nextSegmentIndex]);
        nextSegment.transform.position = currentSegment.transform.position + new Vector3(Constants.LANE_LENGTH, 0f, 0f);

        if (previousSegment) Util.DeactivateAndDestroy(previousSegment);
        previousSegment = currentSegment;
        currentSegment = nextSegment;
    }
}
