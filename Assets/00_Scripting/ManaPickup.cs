﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPickup : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.SendMessage("PickupMana", Constants.MANAPICKUP_AMOUNT);
        Util.DeactivateAndDestroy(gameObject);
    }
}
