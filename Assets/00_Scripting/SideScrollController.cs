﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideScrollController : MonoBehaviour
{
    [SerializeField] private FloatVariable scrollSpeed;

    private bool isScrolling = false;


    public void StartScrolling()
    {
        isScrolling = true;
    }

    public void StopScrolling()
    {
        isScrolling = false;
    }

    private void FixedUpdate()
    {
        if (isScrolling)
        {
            this.transform.position =
                this.transform.position
                + new Vector3(scrollSpeed.value, 0, 0) * Time.fixedDeltaTime;
        }
    }

}
