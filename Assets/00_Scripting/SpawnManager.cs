﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.PlayerInputManager;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private PlayerInputManager playerInputManager;

    [SerializeField] private GameEvent startGameEvent;
    [SerializeField] private GameObject movementRoot;

    [SerializeField] private Transform davidSpawnPoint;
    [SerializeField] private GameObject davidPrefab;

    [SerializeField] private Transform goliathSpawnPoint;
    [SerializeField] private GameObject goliathPrefab;

    private int currentPlayerCount = 0;
    private InputDevice davidInputDevice = null;

    private WaitForSeconds startGameDelay = new WaitForSeconds(1f);


    private void Awake()
    {
        spawnDavid();
        spawnGoliath();
        StartCoroutine(startGame());
    }

    private IEnumerator startGame()
    {
        yield return startGameDelay;
        startGameEvent.Invoke();
    }


    public void OnPlayerJoined()
    {
        Debug.Log("Player Joined!");
    }




    public void OnSpawnPlayer()
    {
        if (Constants.DEBUG) Debug.Log("Player Spawn initiated");

        if (currentPlayerCount == 0) spawnDavid();
        else if (currentPlayerCount == 1) spawnGoliath();

        if (currentPlayerCount == Constants.MAX_PLAYERS)
        {
            startGameEvent?.Invoke();
            GetComponent<PlayerInput>().enabled = false;
        }
    }

    private void spawnDavid()
    {
        if (Constants.DEBUG) Debug.Log("David Spawn Initiated");

        PlayerInput david = PlayerInput.Instantiate(
        davidPrefab,
        pairWithDevice: Keyboard.current
        );
        david.transform.position = davidSpawnPoint.position;
        david.transform.SetParent(movementRoot.transform);

        if (Constants.DEBUG) Debug.Log("Spawned David with Keyboard");

    }

    private void spawnGoliath()
    {
        if (Constants.DEBUG) Debug.Log("Goliath Spawn Initiated");

        PlayerInput goliath = PlayerInput.Instantiate(
        goliathPrefab,
        pairWithDevice: Keyboard.current
        );
        goliath.transform.position = goliathSpawnPoint.position;
        goliath.transform.SetParent(movementRoot.transform);

        if (Constants.DEBUG) Debug.Log("Spawned Goliath with Keyboard");

        ++currentPlayerCount;
    }
}
