﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowFollow : MonoBehaviour
{
    private Transform follow;

    private void Awake()
    {
        follow = transform.parent;
    }
    private void Update()
    {
        transform.position = new Vector3(
            transform.position.x,
            Constants.SHADOW_HEIGHT,
            transform.position.z);
    }


}
